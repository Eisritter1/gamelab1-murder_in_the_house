﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Route : MonoBehaviour {

    Transform[] childObjects;                                                         //Alle childObjects sollen Teil dieses Arrays sein.
    public List<Transform> childFieldList = new List<Transform>();                    //Besserer Zugang zwischen Transform-Array und der Liste.

    void OnDrawGizmos(){

        Gizmos.color = Color.green;

        FillFields();

        for (int i = 0; i < childFieldList.Count; i++){

            Vector3 aktuellePos = childFieldList[i].position;

            if (i > 0){

                Vector3 vorherigePos = childFieldList[i - 1].position;
                Gizmos.DrawLine(vorherigePos, aktuellePos);}}}



    void FillFields(){

        childFieldList.Clear();
        childObjects = GetComponentsInChildren<Transform>();

        foreach(Transform child in childObjects){

            if (child != this.transform){

                childFieldList.Add(child);}}}}

