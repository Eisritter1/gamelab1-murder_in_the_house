﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FWGameManager : MonoBehaviour
{
    public bool gameOver = false;

    //timer:

    public float timer, timeLeft;

    public float timerStart;

    //UI:

    public Text timerText;

    public Text moCounterText;

    public GameObject nothingToSee;

    public GameObject gameOverPanel;

    //Murder object choice:

    public bool knife = true;

    public bool inBlock = false;

    public bool frozenHead = true;

    public bool poison = true;

    public int MOCounter = 0;

    public GameObject[] knifeBlock = new GameObject[2];

    public GameObject[] poisons = new GameObject[4];

    //Audio Management:

    private AudioSource playAudio;

    public AudioClip ticking;

    public AudioClip choir;

    //Scripts:

    private GameManager gm;

    private Clues cl;

    // Start is called before the first frame update:

    void Start()
    {

    }

    private void Awake()
    {
        if (GameObject.Find("GameManager"))
        {
            gm = GameObject.Find("GameManager").GetComponent<GameManager>();
            cl = gm.gameObject.GetComponent<Clues>();
        }

        DontDestroyOnLoad(this);

        gameOverPanel.SetActive(false);

        timer = timeLeft = 45.0f;
        timerStart = Time.time;
        playAudio = GetComponent<AudioSource>();
        ChooseMurderObjects();
        InstantiateKnifeBlock();
        SelectPoisons();
    }

    // Update is called once per frame and runs the timer whilst also checking the amount of found murder objects.

    private void Update()
    {
        //timer:

            if (timeLeft > 0 && !gameOver)
            {
                timeLeft = timer - (Time.time - timerStart);

                string minutes = ((int)timeLeft / 60).ToString();

                string seconds = ((int)timeLeft % 60).ToString();

                timerText.text = "Time left: " + minutes + ":" + seconds + "s";
            }

            else if (timeLeft <= 0 && gm.inMinigame == true)
            {
                timeLeft = 0;
                timerText.text = "Time left: 0:00s";
                gameOver = true;
            
                gameOverPanel.SetActive(true);
            }

            else if (gameOver && gm.inMinigame == true)
            {
                string minutes = ((int)timeLeft / 60).ToString();

                string seconds = ((int)timeLeft % 60).ToString();

                timerText.text = "Time left: " + minutes + ":" + seconds + "s";

                gameOverPanel.SetActive(true);
            }

            //Murder object counter:

            moCounterText.text = "Objects found: " + MOCounter.ToString() + "/3";

            if (MOCounter >= 3)
            {
                gameOver = true;
            }

            //Audio:

            if (!gameOver && !playAudio.isPlaying)
            {
                playAudio.clip = ticking;
                playAudio.Play();
            }

            else if (gameOver && !playAudio.isPlaying && MOCounter < 3)
            {
                playAudio.clip = choir;
                playAudio.Play();
            }

            if (gameOver)
            {
                playAudio.Stop();
            }          
    }

    //Method to select the murder objects that will appear in the minigame

    void ChooseMurderObjects()
    {
        int number = Random.Range(1, 3);
        switch (number)
        {
            case 1:
                break;
            case 2:
                inBlock = true;
                break;
        }
    }

    //InstantiateKnifeBlock gets rid of the wrong knife block if it's supposed to be bloody or not

    void InstantiateKnifeBlock()
    {
        switch (knife && inBlock)
        {
            case true:
                Destroy(knifeBlock[0]);
                break;
            case false:
                Destroy(knifeBlock[1]);
                break;
        }
    }

    //Method to have only one poison selected randomly in the game.

    private void SelectPoisons()
    {
        int number = Random.Range(1, 5);

        for (int i = 0; i < poisons.Length; i++)
        {
            if (i + 1 != number)
            {
                Destroy(poisons[i]);
            }
        }
    }

    public void ReturnToBoard()
    {
        SceneManager.UnloadSceneAsync("FindWaldo Minigame");
        gm.textBox.SetActive(true);
        gm.board.SetActive(true);
        gm.fieldDisplay.gameObject.transform.parent.gameObject.SetActive(true);
        gm.inMinigame = false;
        cl.DiscoverClue(gm.currentPlayer);
        Cursor.lockState = CursorLockMode.Locked;
    }
}
