﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FindWaldoMenu : MonoBehaviour
{
    // Start is called before the first frame update
    public void LaunchMiniGame()
    {
        Debug.Log("Launching Minigame");
        SceneManager.LoadScene("FindWaldo Minigame", LoadSceneMode.Additive);
        SceneManager.UnloadScene("FindWaldo Lobby");
    }
}
