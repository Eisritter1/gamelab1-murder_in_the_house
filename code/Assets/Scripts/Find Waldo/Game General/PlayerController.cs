﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Vector2 target;

    private FWGameManager fwgm;

    // Start is called before the first frame update
    private void Start()
    {
        fwgm = GameObject.Find("FWMinigameManager").GetComponent<FWGameManager>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (!fwgm.gameOver)
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            target = new Vector2(mousePos.x, mousePos.y);

            transform.position = Vector2.MoveTowards(transform.position, target, Time.deltaTime * 100f);
        }
    }
}
