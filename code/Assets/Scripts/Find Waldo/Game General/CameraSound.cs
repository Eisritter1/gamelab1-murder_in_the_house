﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSound : MonoBehaviour
{
    private FWGameManager fwgm;

    private AudioSource cameraSound;

    // Start is called before the first frame update
    void Start()
    {
        cameraSound = GetComponent<AudioSource>();
        fwgm = GameObject.Find("FWMinigameManager").GetComponent<FWGameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (fwgm.gameOver)
        {
            cameraSound.Stop();
        }
    }
}
