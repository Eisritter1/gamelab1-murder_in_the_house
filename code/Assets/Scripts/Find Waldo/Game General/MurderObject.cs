﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MurderObject : MonoBehaviour
{
    //Script communication:

    private FWGameManager fwgm;


    //variables:

    private bool found = false;

    private Material highlight;


    //Audio:

    public AudioClip findSound;

    public AudioClip brushSound;

    private AudioSource MoAudio;

    // Start is called before the first frame update
    void Start()
    {
        fwgm = GameObject.Find("FWMinigameManager").GetComponent<FWGameManager>();
        MoAudio = GetComponent<AudioSource>();
        highlight = GetComponent<SpriteRenderer>().material;
    }

    private void OnMouseDown()
    {
        if (!fwgm.gameOver)
        {
            if (!found)
            {
                MoAudio.PlayOneShot(findSound, 0.8f);
                fwgm.MOCounter = fwgm.MOCounter + 1;
                found = true;
                Debug.Log(gameObject.name);
            }
        }
    }

    private void OnMouseEnter()
    {
        if (!fwgm.gameOver && !found)
        {
            MoAudio.PlayOneShot(brushSound, 0.8f);
            highlight.SetFloat("Thickness", 0.2f);
            highlight.SetColor("_Color", Color.yellow);
        }
    }

    private void OnMouseExit()
    {
        if (!fwgm.gameOver)
        {
            highlight.SetFloat("Thickness", 0.0f);
            highlight.SetColor("_Color", Color.black);
        }
    }
}
