﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackArrow : MonoBehaviour
{
    private FWGameManager fwgm;

    [SerializeField]                    //nächstes privates Objekt im Unity Editor zu sehen.
    private GameObject drawer;

    [SerializeField]
    private GameObject kitchen;

    private void Start()
    {
        fwgm = GameObject.Find("FWMinigameManager").GetComponent<FWGameManager>();
    }

    private void OnMouseDown()
    {
        if (!fwgm.gameOver)
        {
            Debug.Log("Returning to Kitchen");
            kitchen.SetActive(true);
            drawer.SetActive(false);

            fwgm.nothingToSee.SetActive(false);
        }
    }
}
