﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawerBehaviour : MonoBehaviour
{
    public GameObject[] drawers = new GameObject[6];

    public GameObject[] drawerContent = new GameObject[4];

    private bool retry = true;

    private FWGameManager fwgm;

    // Start is called before the first frame update
    void Start()
    {
        fwgm = GameObject.Find("FWMinigameManager").GetComponent<FWGameManager>();
        ChooseDrawerContent();
    }

    void ChooseDrawerContent()
    {
        bool hasKnives = false;
        bool hasPlates = false;
        bool hasCutlery = false;
        bool hasTinfoil = false;
        bool isEmpty = false;

        if (fwgm.knife = true && fwgm.inBlock == false)
        {
            int number = Random.Range(0, 4);

            drawerContent[number] = drawers[5];
            hasKnives = true;
        }

        for (int i = 0; i < drawerContent.Length; i++)
        {
            retry = true;
            while (retry)
            {
                retry = false;
                int number = Random.Range(1, 6);
                if (drawerContent[i] != drawers[5])
                {
                    switch (number)
                    {
                        case 1:
                            if (hasPlates == false)
                            {
                                drawerContent[i] = drawers[0];
                                hasPlates = true;
                            }
                            break;
                        case 2:
                            if (hasCutlery == false)
                            {
                                drawerContent[i] = drawers[1];
                                hasCutlery = true;
                            }
                            break;
                        case 3:
                            if (hasKnives == false)
                            {
                                drawerContent[i] = drawers[2];
                                hasKnives = true;
                            }
                            break;
                        case 4:
                            if (hasTinfoil == false)
                            {
                                drawerContent[i] = drawers[3];
                                hasTinfoil = true;
                            }
                            break;
                        case 5:
                            if (isEmpty == false)
                            {
                                drawerContent[i] = drawers[4];
                                isEmpty = true;
                            }
                            break;
                        default:
                            retry = true;
                            break;
                    }
                }
            }
        }
    }
}
