﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drawer1 : MonoBehaviour
{
    //Script Communication:

    private FWGameManager fwgm;

    private DrawerBehaviour db;


    //Variables:

    private GameObject drawer;

    [SerializeField]
    private GameObject kitchen;

    private Material highlight;

    private void Start()
    {
        db = GameObject.Find("Drawers").GetComponent<DrawerBehaviour>();
        fwgm = GameObject.Find("FWMinigameManager").GetComponent<FWGameManager>();
        highlight = GetComponent<SpriteRenderer>().material;
    }

    private void OnMouseDown()
    {
        if (!fwgm.gameOver)
        {
            drawer = db.drawerContent[0];
            Debug.Log("Opening " + drawer.name);

            drawer.SetActive(true);
            kitchen.SetActive(false);

            if (drawer.name != "Bloodstained Sharp Knives")
            {
                fwgm.nothingToSee.SetActive(true);
            }
        }
    }

    private void OnMouseEnter()
    {
        if (!fwgm.gameOver)
        {
            highlight.SetFloat("Thickness", 0.2f);
            highlight.SetColor("_Color", Color.yellow);
        }
    }

    private void OnMouseExit()
    {
        if (!fwgm.gameOver)
        {
            highlight.SetFloat("Thickness", 0.0f);
            highlight.SetColor("_Color", Color.black);
        }
    }
}
