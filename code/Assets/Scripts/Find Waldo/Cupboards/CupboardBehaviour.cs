﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CupboardBehaviour : MonoBehaviour
{
    public GameObject[] cupboards = new GameObject[5];

    public GameObject[] cupboardContent = new GameObject[5];

    bool retry = true;

    // Start is called before the first frame update
    void Start()
    {
        ChooseCupboardContent();
    }

    private void ChooseCupboardContent()
    {
        bool hasSpices = false;
        bool hasPots = false;
        bool hasSnacks = false;
        bool hasGlasses = false;
        bool isEmpty = false;

        for (int i = 0; i < cupboardContent.Length; i++)
        {
            retry = true;
            while (retry)
            {
                int number = Random.Range(1, 6);
                switch (number)
                {
                    case 1:
                        if (!hasSpices)
                        {
                            cupboardContent[i] = cupboards[0];
                            hasSpices = true;
                            retry = false;
                        }
                        break;
                    case 2:
                        if (!isEmpty)
                        {
                            cupboardContent[i] = cupboards[1];
                            isEmpty = true;
                            retry = false;
                        }
                        break;
                    case 3:
                        if (!hasGlasses)
                        {
                            cupboardContent[i] = cupboards[2];
                            hasGlasses = true;
                            retry = false;
                        }
                        break;
                    case 4:
                        if (!hasSnacks)
                        {
                            cupboardContent[i] = cupboards[3];
                            hasSnacks = true;
                            retry = false;
                        }
                        break;
                    case 5:
                        if (!hasPots)
                        {
                            cupboardContent[i] = cupboards[4];
                            hasPots = true;
                            retry = false;
                        }
                        break;
                }
            }
        }
    }
}
