﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinkCupboard : MonoBehaviour
{
    //Script Communication:

    private FWGameManager fwgm;

    
    //Variables:

    [SerializeField]                    //nächstes privates Objekt im Unity Editor zu sehen.
    private GameObject cupboard;

    [SerializeField]
    private GameObject kitchen;

    private Material highlight;

    private void Start()
    {
        fwgm = GameObject.Find("FWMinigameManager").GetComponent<FWGameManager>();
        highlight = GetComponent<SpriteRenderer>().material;
    }

    private void OnMouseDown()
    {
        if (!fwgm.gameOver)
        {
            Debug.Log("Opening " + cupboard.name);

            cupboard.SetActive(true);
            kitchen.SetActive(false);

            fwgm.nothingToSee.SetActive(true);
        }
    }

    private void OnMouseEnter()
    {
        if (!fwgm.gameOver)
        {
            highlight.SetFloat("Thickness", 0.2f);
            highlight.SetColor("_Color", Color.yellow);
        }
    }

    private void OnMouseExit()
    {
        if (!fwgm.gameOver)
        {
            highlight.SetFloat("Thickness", 0.0f);
            highlight.SetColor("_Color", Color.black);
        }
    }
}
