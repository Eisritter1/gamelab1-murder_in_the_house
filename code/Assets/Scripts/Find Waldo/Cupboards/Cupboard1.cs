﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cupboard1 : MonoBehaviour
{
    private FWGameManager fwgm;

    private CupboardBehaviour cb;

    [SerializeField]
    private GameObject cupboard;

    [SerializeField]
    private GameObject kitchen;

    private Material highlight;

    private void Start()
    {
        cb = GameObject.Find("Cupboards").GetComponent<CupboardBehaviour>();
        fwgm = GameObject.Find("FWMinigameManager").GetComponent<FWGameManager>();
        highlight = GetComponent<SpriteRenderer>().material;
    }

    private void OnMouseEnter()
    {
        if (!fwgm.gameOver)
        {
            highlight.SetFloat("Thickness", 0.2f);
            highlight.SetColor("_Color", Color.yellow);
        }
    }

    private void OnMouseExit()
    {
        if (!fwgm.gameOver)
        {
            highlight.SetFloat("Thickness", 0.0f);
            highlight.SetColor("_Color", Color.black);
        }
    }

    private void OnMouseDown()
    {
        if (!fwgm.gameOver)
        {
            cupboard = cb.cupboardContent[0];
            Debug.Log("Opening " + cupboard.name);

            cupboard.SetActive(true);                           //Changes the scenery from the kitchen to the cupboard's inside
            kitchen.SetActive(false);

            if (cupboard.name != "Spices Cupboard")
            {
                fwgm.nothingToSee.SetActive(true);
            }
        }
    }
}
