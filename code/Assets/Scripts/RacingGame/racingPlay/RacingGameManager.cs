﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RacingGameManager : MonoBehaviour
{
    //Scripts:

    private GameManager gm;

    //variables:

    private float timer, timeLeft;

    public bool mgameOver = false;

    public float completion;

    public int completionPercent;

    public int maxAdvance;

    public float timerStart;

    //UI:

    public Text timerText;

    public GameObject gameOverPanel;

    public Text gameOverText;

    public Text advancementDisplay;

    private void Awake()
    {
        if (GameObject.Find("GameManager"))
        {
            gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        }

        DontDestroyOnLoad(this);

        gameOverPanel.SetActive(false);

        timer = timeLeft = 30.0f;
        maxAdvance = Random.Range(5, 9);                          //For build please change to 5, 9!!

        timerStart = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        string displayTime = ((int)timeLeft % 60).ToString();

        string advance = ((int)(completion * maxAdvance) / 100).ToString();

        if (advance == "0")
        {
            advance = "1";
        }

        if (timeLeft > 0 && !mgameOver)
        {
            timeLeft = timer - (Time.time - timerStart);

            timerText.text = "Time left: " + displayTime + "s";
        } else if (timeLeft <= 0)
        {
            timeLeft = 0;
            timerText.text = "Time left: 0s";
            mgameOver = true;
        }

        advancementDisplay.text = "Advance: " + advance + " field/s";
        
        completion = ((30 - timeLeft) * 100) / timer;

        completionPercent = (int)completion;
        
        if (mgameOver && gm.inMinigame)
        {
            timerText.text = "Time left: " + displayTime + "s";
            
            if (gameOverPanel)
            {
                gameOverPanel.SetActive(true);
            }

            gameOverText.text = "Game Over!\nMinigame completed to " + completionPercent.ToString() + " percent!";
        }

        
    }

    public void ReturnToBoard()
    {
        SceneManager.UnloadScene("RacingPlay");
        gm.board.SetActive(true);
        gm.inMinigame = false;
    }
}
