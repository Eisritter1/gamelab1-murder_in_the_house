﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTrack : MonoBehaviour {

    public float speed;
    Vector2 offset;

    private RacingGameManager rgm;

    // Start is called before the first frame update
    void Start()
    {
        rgm = GameObject.Find("Minigame Manager").GetComponent<RacingGameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!rgm.mgameOver)
        {
            offset = new Vector2(0, Time.time * speed);
            GetComponent<Renderer>().material.mainTextureOffset = offset;
        }
    }
}
