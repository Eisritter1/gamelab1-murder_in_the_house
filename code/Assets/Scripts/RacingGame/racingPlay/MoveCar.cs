﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCar : MonoBehaviour {

    public AudioManager am;
    public float carSpeed;
    public float carPos = 3.32f;
    Vector3 position;

    private RacingGameManager rgm;

    // Start is called before the first frame update
    void Start()
    {
        am.carSound.Play();

        position = transform.position;
        rgm = GameObject.Find("Minigame Manager").GetComponent<RacingGameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!rgm.mgameOver)
        {
            position.x += Input.GetAxis("Horizontal") * carSpeed * Time.deltaTime;
            position.x = Mathf.Clamp(position.x, -4.25f, 4.5f);
            transform.position = position;
        }

        if (rgm.mgameOver)
        {
            am.carSound.Stop();
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Enemy Car #1")
        {
            Destroy(gameObject);
            rgm.mgameOver = true;
            am.carSound.Stop();
        }
    }
}
