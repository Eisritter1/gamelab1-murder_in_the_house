﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSpawn : MonoBehaviour
{
    public GameObject[] enemyCars;
    int carNo;
    public float maxPos = 2.75f;
    public float spawnTimer = 0.5f;
    float timer;

    private RacingGameManager rgm;

    void Start()
    {
        timer = spawnTimer;
        rgm = GameObject.Find("Minigame Manager").GetComponent<RacingGameManager>();
    }


    void Update()
    {
        if (!rgm.mgameOver)
        {
            timer -= Time.deltaTime;

            if (timer <= 0)
            {
                Vector3 carPos = new Vector3(Random.Range(-3.7f, 3.7f), transform.position.y, transform.position.z);
                carNo = Random.Range(0, 5);
                Instantiate(enemyCars[carNo], carPos, transform.rotation);
                timer = spawnTimer;
            } 
        }
    }
}
