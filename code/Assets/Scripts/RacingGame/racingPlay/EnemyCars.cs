﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCars : MonoBehaviour
{
    private RacingGameManager rgm;

    public float speed = 5f;

    void Start()
    {
        rgm = GameObject.Find("Minigame Manager").GetComponent<RacingGameManager>();
    }

    void Update()
    {
        if (!rgm.mgameOver)
        {
            transform.Translate(new Vector3(0, 1, 0) * speed * Time.deltaTime);
        }
    }
}
