﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class GameManager : MonoBehaviour
{
    //Minigame Script Imports:

    public GameObject racingManager;
    private RacingGameManager rgm;

    public GameObject fwManager;
    private FWGameManager fwgm;


    //Minigame Variables:

    public int advance = 1;

    public float completion = 1;

    //Global Variables:

        //GameObjects:

        public GameObject board;

        public GameObject[] playerCharacters;
      
        //UI:

        public Text movementText;

        public Text skipText;

        public Text fieldDisplay;

        public Text roundDisplay;

        public Text nextFWDisplay;

        public GameObject textBox;

        public GameObject roundCounter;

        public GameObject buttons;

        public GameObject pauseMenu;

        public GameObject launchFWMinigame;

        //regular Variables:

        //booleans:

        public bool gameOver = false;

        public bool tutorial = true;

        public bool inGame = false;

        public bool inMinigame = false;

        public bool inSusMenu = false;

        private bool inTutorial = false;

        public bool paused = false;

        public bool inTileEffects = false;

        public bool moving = false;

        public bool newRound = true;

        public bool inRound = false;

        public bool[,] fwPlayed;

        //ints:

        public int roundSteps = 7;

        public int currentRound = 1;

        private int introIndex = 0;

        private int tutorialIndex = 0;

        public int gameIndex = 0;

        public int players = 0;

        public int nAI = 0;

        public int playerTurn = 1;

        public int[] skipTurns;

        public int currentPlayer = 1;

        public int[] findWaldoTiles;

        int[] findWaldoCounter;

        int maxAdvance;

        int moveDist;

        int fwdist = 0;

    //strings

    private string[] introText;

        private string[] tutorialText;

        private string[] gameText;

        public string difficulty;

        //Murderer variables:

        public string deathRoom;

        public string murderWeapon;

        private string killedBy;

        public string culprit;

        private string[] playerState;

        //Scripts:

        private MovePlayer mp1;
        private MovePlayer mp2;
        private MovePlayer mp3;
        private MovePlayer mp4;

        private MovePlayer mp;

        private MouseLook ml1;
        private MouseLook ml2;
        private MouseLook ml3;
        private MouseLook ml4;

        public MouseLook ml;

        private MainMenu mm;

        private Clues c;

    //Tile effects:

    string[] tileEffects;

    int normalCounter = 0;

    int plus1Counter = 0;

    int plus2Counter = 0;

    int plus3Counter = 0;

    int minus3Counter = 0;

    int minus2Counter = 0;

    int minus1Counter = 0;

    int hitmanCounter = 0;

    int minigameCounter = 0;

    int testimonyCounter = 0;

    int interrogationCounter = 0;

    private void Start()
    {
        mp1 = playerCharacters[0].GetComponent<MovePlayer>();
        mp2 = playerCharacters[1].GetComponent<MovePlayer>();
        mp3 = playerCharacters[2].GetComponent<MovePlayer>();
        mp4 = playerCharacters[3].GetComponent<MovePlayer>();

        mp = mp1;

        ml1 = GameObject.Find("Player1 Camera").GetComponent<MouseLook>();
        ml2 = GameObject.Find("Player2 Camera").GetComponent<MouseLook>();
        ml3 = GameObject.Find("Player3 Camera").GetComponent<MouseLook>();
        ml4 = GameObject.Find("Player4 Camera").GetComponent<MouseLook>();

        ml = ml1;

        c = this.gameObject.GetComponent<Clues>();

        ml.enabled = false;
        ml1.enabled = false;
        ml2.enabled = false;
        ml3.enabled = false;
        ml4.enabled = false;

        ml2.gameObject.SetActive(false);
        ml3.gameObject.SetActive(false);
        ml4.gameObject.SetActive(false);
        
        buttons.gameObject.SetActive(false);

        tileEffects = new string[mp1.currentRoute.childFieldList.Count];

        Cursor.lockState = CursorLockMode.Locked;

        findWaldoTiles = new int[] { 0, 0, 0, 0 };

        //default player states: 1 player 3 AIs
        playerState = new string[] { "p", "ai", "ai", "ai" };

        skipTurns = new int[] { 0, 0, 0, 0 };

        findWaldoCounter = new int[] { 0, 0, 0, 0 };

        fwPlayed = new bool[4, mp.currentRoute.childFieldList.Count];

        DeathCircumstances();

        SetTileEffects();

        //Dialogue options for the introduction
        introText = new string[] {"<b>Commissioner</b>: A murder has been reported in this house!", "<b>Commissioner</b>: Yesterday, <color=red>Sir Joe</color> hosted a party with five guests.",
        "<b>Commissioner</b>: He was found dead this morning in his " + deathRoom + ", killed by " + killedBy, "<b>Commissioner</b>: The only suspects are the five guests and his chef, the only people present that night.",
        "<b>Commissioner</b>: <color=red>Candace Chase</color>, his wife. Many say they both couldn't stand each other though.", "<b>Commissioner</b>: His nephew, <color=red>Steve</color>. He and his uncle were very fond of each other, all the way to include him in his rich will and testament!",
        "<b>Commissioner</b>: <color=red>Karen Johnson</color>, Sir Joe's sister. According to reports, she never got over the fact he inherited their parents' house.", "<b>Commissioner</b>: <color=red>Stabby Mike</color>, a very old friend of Sir Joe's. There are rumours he broke out of prison, where he was convicted of stabbing countless people.",
        "<b>Commissioner</b>: <color=red>Sir Winston of Blackburn</color>, his business partner. The two of them earned their Lordship together for their deeds.", "<b>Commissioner</b>: <color=red>Tom Baker</color>, his chef. It was a common occurence for Sir Joe's chefs to leave due to underpayment.",
        "<b>Commissioner</b>: Anyways, would you like a quick briefing of how the investigation will work?"};

        //Dialogue text for the tutorial
        tutorialText = new string[] { "<b>Commissioner</b>: Alright, let me give you a brief overview of how this works.", "<b>Commissioner</b>: Each round consists of three simple parts:",
        "<b>Commissioner</b>: 1. <color=green>Minigames</color> to decide how far you can move! The better you complete it, the further you will go.", "<b>Commissioner</b>: 2. <color=green>Movement forwards</color>: You will move forward to a specific tile.",
        "<b>Commissioner</b>: 3. <color=green>Tile effects</color>: If the tile you landed on has any effects, they will activate in the 3rd and last part of your turn!", "There are many different effects, such as +2 tiles, interrogations where you will miss 1-3 turns...",
        "... or even the hitman who will send a random player back 3-5 spaces!", "In each room, you will be facing another minigame in order to find clues on the culprit. You cannot leave the room unless you have completed that minigame.",
        "If you click on your right mouse button, you'll open the suspects and clues overview, to keep track of anything you need!", "The clues and suspects overview is also accessible in the game's <color=green>menu</color>.",
        "When you will have searched each room, you will present your clues and accuse a suspect.", "If you are right, you win the game. If you are wrong, you will be fired immediately.", "Beware though! Other detectives will be competing for the glory of catching the culprit besides you!",
        "Alright, that's it! Best of luck comrade, and may the best detective win!"};
    }

    private void Update()
    {
        switch(currentPlayer)
        {
            case 1:
                mp = mp1;
                ml = ml1;
                ml1.gameObject.SetActive(true);
                ml4.gameObject.SetActive(false);
                break;
            case 2:
                mp = mp2;
                ml = ml2;
                ml2.gameObject.SetActive(true);
                ml1.gameObject.SetActive(false);
                break;
            case 3:
                mp = mp3;
                ml = ml3;
                ml3.gameObject.SetActive(true);
                ml2.gameObject.SetActive(false);
                break;
            case 4:
                mp = mp4;
                ml = ml4;
                ml4.gameObject.SetActive(true);
                ml3.gameObject.SetActive(false);
                break;
        }

        //Search for a Racing Game Minigame Manager:
        if (GameObject.Find("Minigame Manager") && !inMinigame)
        {
            racingManager = GameObject.Find("Minigame Manager");
            rgm = racingManager.GetComponent<RacingGameManager>();
            advance = (int) (rgm.maxAdvance * (rgm.completion / 100));              //Save its completion value in the "advance" variable.
            completion = (int) rgm.completion;
            maxAdvance = rgm.maxAdvance;
            Destroy(racingManager);
        }

        if (advance == 0)
        {
            advance = 1;
        }
        
        //Search for a Find Waldo Minigame Manager GameObject:
        if (GameObject.Find("FWMinigameManager"))
        {
            fwManager = GameObject.Find("FWMinigameManager");
            fwgm = fwManager.GetComponent<FWGameManager>();
            if (!inMinigame)
            {
                Destroy(fwManager);
            }
        }

        //Search for the main menu manager:
        if (GameObject.Find("Main Manager"))
        {
            mm = GameObject.Find("Main Manager").GetComponent<MainMenu>();          //Save the number of players & difficulty, then delete the manager.
            players = mm.players;
            switch (players)
            {
                case 1:
                    playerState[0] = "p";
                    break;
                case 2:
                    playerState[0] = playerState[1] = "p";
                    break;
                case 3:
                    playerState[0] = playerState[1] = playerState[2] = "p";
                    break;
                case 4:
                    playerState[0] = playerState[1] = playerState[2] = playerState[3] = "p";
                    break;
            }
            difficulty = mm.difficulty;
            Destroy(GameObject.Find("Main Manager"));
        }

        //while in the intro section of the game:
        if (!inGame && !paused)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                SkipIntro();
            }

            fieldDisplay.gameObject.transform.parent.gameObject.SetActive(false);

            //Intro before tutorial:
            if (!inTutorial)
            {
                //Introduction to the game:
                if (Input.GetKeyDown(KeyCode.Space) && introIndex < introText.Length)
                {
                    movementText.text = introText[introIndex];
                    introIndex++;
                }

                //Scroll back in the intro
                if (Input.mouseScrollDelta.y > 0 && introIndex > 0)
                {
                    introIndex--;
                    movementText.text = introText[introIndex];
                }
            }

            if (introIndex < introText.Length)
            {
                buttons.SetActive(false);
            }

            //Tutorial choice button activation:
            if (introIndex == introText.Length)
            {
                Cursor.lockState = CursorLockMode.None;
                buttons.gameObject.SetActive(true);
                skipText.text = "Press space to skip.";
            }

            //Tutorial choice button deactivation and transition to tutorial:
            if (introIndex > introText.Length)
            {
                Cursor.lockState = CursorLockMode.Locked;
                buttons.gameObject.SetActive(false);
                inTutorial = true;
            }

            //tutorial (or maybe not)
            if (inTutorial)
            {
                //no tutorial:
                if (!tutorial && tutorialIndex < tutorialText.Length - 1)
                {
                    movementText.text = "Sheesh! okay, okay... right this way Mr. Professional.";
                    tutorialIndex = tutorialText.Length;
                }
        
                //tutorial:
                if (Input.GetKeyDown(KeyCode.Space) && tutorial && tutorialIndex < tutorialText.Length)
                {
                    movementText.text = tutorialText[tutorialIndex];
                    tutorialIndex++;
                }

                //scroll back in tutorial:
                if (Input.mouseScrollDelta.y > 0 && tutorial && tutorialIndex > 0)
                {
                    tutorialIndex--;
                    movementText.text = tutorialText[tutorialIndex];
                }
            }

            if (tutorialIndex == tutorialText.Length && Input.GetKeyDown(KeyCode.Space))
            {
                inGame = true;
                fieldDisplay.gameObject.transform.parent.gameObject.SetActive(true);
            }
        }

        //While in the game:
        if (inGame && !paused && !moving && !inTileEffects)
        {
            //while the game isn't over:
            if (!gameOver)
            {
                if (newRound && !inRound)
                {
                    textBox.SetActive(false);
                    roundCounter.SetActive(true);
                    roundDisplay.text = "Round " + currentRound + "!";
                }

                if(playerState[currentPlayer - 1] == "p")
                {
                    PlayTurn(currentPlayer - 1);
                } else
                {
                    SimTurn(currentPlayer - 1);
                }
            }
        }

        //Deletion of leftover clones from the minigame
        if (!inMinigame)
        {
            Destroy(GameObject.Find("Enemy Car #1(Clone)"));
            Destroy(GameObject.Find("Enemy Car #2(Clone)"));
            Destroy(GameObject.Find("Enemy Car #3(Clone)"));
            Destroy(GameObject.Find("Enemy Car #4(Clone)"));
            Destroy(GameObject.Find("Enemy Car #5(Clone)"));
            Destroy(GameObject.Find("Enemy Car #6(Clone)"));
            Destroy(GameObject.Find("Enemy Car #7(Clone)"));
        }

        //Pauses the game:

        if (Input.GetKeyDown(KeyCode.Escape) && !paused)
        {
            PauseGame();
            Time.timeScale = 0;
        } else if (Input.GetKeyDown(KeyCode.Escape) && paused)
        {
            UnpauseGame();
        }                                                                                            

        if (paused == false)
        {
            Time.timeScale = 1;
        }

        //Update of the fields display:
        fieldDisplay.text = "current: " + tileEffects[mp.routePosition] + " | in 1: " + tileEffects[mp.routePosition + 1] + " | in 2: " + tileEffects[mp.routePosition + 2] + " | in 3: " + tileEffects[mp.routePosition + 3] + " | in 4: " + tileEffects[mp.routePosition + 4] + " | in 5: " + tileEffects[mp.routePosition + 5] + " | in 6: " + tileEffects[mp.routePosition + 6];

        if(findWaldoTiles[findWaldoCounter[currentPlayer - 1]] - mp.routePosition >= 0)
        {
            fwdist = findWaldoTiles[findWaldoCounter[currentPlayer - 1]] - mp.routePosition;
        } else
        {
            findWaldoCounter[currentPlayer - 1]++;
            fwdist = findWaldoTiles[findWaldoCounter[currentPlayer - 1]] - mp.routePosition;
        }
        
        nextFWDisplay.text = "Next Find Waldo tile in: " + fwdist + " tiles.";

        if (mp.routePosition == mp.currentRoute.childFieldList.Count)
        {
            for (int i = 0; i < mp.currentRoute.childFieldList.Count; i++)
            {
                fwPlayed[currentPlayer - 1, i] = false;
            }
        }
    }

    //method to set the activation of the tutorial or not
    public void TutorialBoolean(Button button)
    {
        if (button.gameObject.name == "Tutorial please")
        {
            tutorial = true;
            introIndex++;
        }
        else
        {
            tutorial = false;
            introIndex++;
        }
    }

    //Decides the way, place of the murder as well as the killer
    private void DeathCircumstances()
    {
        int i = Random.Range(1, 6);
        switch (i)
        {
            case 1:
                deathRoom = "<color=red>kitchen</color>";                                                                                                  
                break;
            case 2:
                deathRoom = "<color=red>bathroom</color>";
                break;
            case 3:
                deathRoom = "<color=red>bedroom</color>";
                break;
            case 4:
                deathRoom = "<color=red>dining room</color>";
                break;
            case 5:
                deathRoom = "<color=red>lounge</color>";
                break;
        }

        int j = Random.Range(1, 6);
        switch (j)
        {
            case 1:
                murderWeapon = "dagger";                                                                                                                    
                killedBy = "<color=red>28 stab wounds</color>.";
                break;
            case 2:
                murderWeapon = "poison";
                killedBy = "<color=red>a poisoning</color>. <color=red>Cyanide</color> probably.";
                break;
            case 3:
                murderWeapon = "gun";
                killedBy = "a clean <color=red>gunshot</color> right in the heart.";
                break;
            case 4:
                murderWeapon = "rope";
                killedBy = "<color=red>a strangulation</color> through a <color=red>rope</color>, judging from the marks on his neck.";
                break;
            case 5:
                murderWeapon = "wrench";
                killedBy = "<color=red>a laceration</color> on his <color=red>head</color>, horrible sight.";
                break;
        }

        int k = Random.Range(1, 7);
        switch (k)
        {
            case 1:
                culprit = "<color=red>Candace Chase</color>";
                break;
            case 2:
                culprit = "<color=red>Steve</color>";
                break;
            case 3:
                culprit = "<color=red>Karen</color>";
                break;
            case 4:
                culprit = "<color=red>Mike</color>";
                break;
            case 5:
                culprit = "<color=red>Sir Winston</color>";
                break;
            case 6:
                culprit = "<color=red>Tom</color>";
                break;
        }
    }

    //method to pause the game:
    public void PauseGame()
    {
        paused = true;
        pauseMenu.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
    }

    //Method to unpause the game:
    public void UnpauseGame()
    {
        paused = false;
        pauseMenu.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
    }

    //function to exit the application / game:
    public void ExitGame()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    //function to set the tile effects.
    void SetTileEffects()
    {
        //Set up of the find waldo tile:
        int findWaldo1 = Random.Range(1, 15);
        tileEffects[findWaldo1] = "<color=red>Find Waldo</color>";
        findWaldoTiles[0] = findWaldo1;
        Debug.Log(findWaldo1 + 1);

        int findWaldo2 = Random.Range(18, 32);
        tileEffects[findWaldo2] = "<color=red>Find Waldo</color>";
        findWaldoTiles[1] = findWaldo2;
        Debug.Log(findWaldo2 + 1);

        int findWaldo3 = Random.Range(35, 49);
        tileEffects[findWaldo3] = "<color=red>Find Waldo</color>";
        findWaldoTiles[2] = findWaldo3;
        Debug.Log(findWaldo3 + 1);

        int findWaldo4 = Random.Range(52, 66);
        tileEffects[findWaldo4] = "<color=red>Find Waldo</color>";

        findWaldoTiles[3] = findWaldo4;
        Debug.Log(findWaldo4 + 1);

        tileEffects[0] = "<color=grey>start</color>";

        tileEffects[68] = "<color=red>accusation</color>";

        for (int i = 0; i < tileEffects.Length; i++)
        {
            if (tileEffects[i] != "<color=red>Find Waldo</color>" && i > 0)
            {
                bool retry = true;
                while (retry)
                {
                    //deciding for the remaining 63/69 tiles:
                    int choice = Random.Range(1, 64);
                    switch (choice)
                    {
                        //In 27/68 cases, the tile will have no effect.
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                        case 10:
                        case 11:
                        case 12:
                        case 13:
                        case 14:
                        case 15:
                        case 16:
                        case 17:
                        case 18:
                        case 19:
                        case 20:
                        case 21:
                        case 22:
                        case 23:
                        case 24:
                        case 25:
                        case 26:
                        case 27:
                            if (normalCounter < 27)
                            {
                                tileEffects[i] = "<color=grey>Normal</color>";
                                normalCounter++;
                                retry = false;
                            }
                            break;
                        //in 5/68 cases, the tile will send the player forward by one tile
                        case 28:
                        case 29:
                        case 30:
                        case 31:
                        case 32:
                            if (plus1Counter < 5)
                            {
                                tileEffects[i] = "<color=green>+1</color>";
                                plus1Counter++;
                                retry = false;
                            }
                            break;
                        //in 4/68 cases, the tile will send the player forward by two tiles
                        case 33:
                        case 34:
                        case 35:
                        case 36:
                            if (plus2Counter < 4)
                            {
                                tileEffects[i] = "<color=green>+2</color>";
                                plus2Counter++;
                                retry = false;
                            }
                            break;
                        //in 3/68 cases, the tile will send the player forward by three tiles
                        case 37:
                        case 38:
                        case 39:
                            if (plus3Counter < 3)
                            {
                                tileEffects[i] = "<color=green>+3</color>";
                                plus3Counter++;
                                retry = false;
                            }
                            break;
                        //in 3/68 cases, the tile will send the player back by three tiles
                        case 40:
                        case 41:
                        case 42:
                            if (minus3Counter < 3)
                            {
                                tileEffects[i] = "<color=orange>-3</color>";
                                minus3Counter++;
                                retry = false;
                            }
                            break;
                        //in 4/68 cases, the tile will send the player back by two tiles
                        case 43:
                        case 44:
                        case 45:
                        case 46:
                            if (minus2Counter < 4)
                            {
                                tileEffects[i] = "<color=orange>-2</color>";
                                minus2Counter++;
                                retry = false;
                            }
                            break;
                        //in 5/68 cases, the tile will send the player back by one tile
                        case 47:
                        case 48:
                        case 49:
                        case 50:
                        case 51:
                            if (minus1Counter < 5)
                            {
                                tileEffects[i] = "<color=orange>-1</color>";
                                minus1Counter++;
                                retry = false;
                            }
                            break;
                        //in 4/68 cases, the tile will send a random player back by 3-5 tiles
                        case 52:
                        case 53:
                        case 54:
                        case 55:
                            if (hitmanCounter < 4)
                            {
                                tileEffects[i] = "<color=grey>Hitman</color>";
                                hitmanCounter++;
                                retry = false;
                            }
                            break;
                        //in 4/68 cases, the tile will activate a minigame between the player and either 1 or 3 other players.
                        case 56:
                        case 57:
                        case 58:
                        case 59:
                            if (minigameCounter < 4)
                            {
                                tileEffects[i] = "<color=yellow>Minigame</color>";
                                minigameCounter++;
                                retry = false;
                            }
                            break;
                        //in 2/68 cases, the tile will  either send the player forward or back by five tiles
                        case 60:
                        case 61:
                            if (testimonyCounter < 2)
                            {
                                tileEffects[i] = "<color=blue>Testimony</color>";
                                testimonyCounter++;
                                retry = false;
                            }
                            break;
                        //in 2/68 cases, the tile will make the player miss 1-3 rounds
                        case 62:
                        case 63:
                            if (interrogationCounter < 2)
                            {
                                tileEffects[i] = "<color=orange>Interrogation</color>";
                                plus2Counter++;
                                retry = false;
                            }
                            break;
                    }
                }
            }
            Debug.Log(tileEffects[i]);
        }
    }

    //function to play the tile effects
    void PlayTileEffects(int n, int player)
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            switch (tileEffects[n])
            {
                case "<color=red>Find Waldo</color>":                       //implemented :D
                    if(fwPlayed[player, n] == false)
                    {
                        if(playerState[player] == "p")
                        {
                            fwPlayed[player, n] = true;
                            Cursor.lockState = CursorLockMode.None;
                            nextFWDisplay.gameObject.transform.parent.gameObject.SetActive(false);
                            launchFWMinigame.SetActive(true);
                            movementText.text = "Ready to open the minigame?";
                            skipText.gameObject.SetActive(false);
                            findWaldoCounter[player]++;
                        }
                        else
                        {
                            movementText.text = "CPU is playing the minigame...";
                            c.DiscoverClue(player);
                        }
                    } else
                    {
                        movementText.text = "You already completed this minigame!";
                    }

                    break;
                case "<color=grey>Normal</color>":                          //not hard to implement :p
                    movementText.text = "lucky you! nothing's happening!";
                    break;
                case "<color=green>+1</color>":                                                  //implemented: D
                    movementText.text = "you move forward by one tile!";
                    mp.MoveFigure(1);
                    break;
                case "<color=green>+2</color>":                                                 //implemented :D
                    movementText.text = "you move forward by two tiles!";
                    mp.MoveFigure(2);
                    break;
                case "<color=green>+3</color>":                                                 //implemented :D
                    movementText.text = "you move forward by three tiles!";
                    mp.MoveFigure(3);
                    break;
                case "<color=orange>-1</color>":                                                 //implemented :D
                    movementText.text = "you move back by one tile!";
                    mp.MoveFigure(-1);
                    break;
                case "<color=orange>-2</color>":                                                 //implemented :D
                    movementText.text = "you move back by two tiles!";
                    mp.MoveFigure(-2);
                    break;
                case "<color=orange>-3</color>":                                                 //implemented :D
                    movementText.text = "you move back by three tiles!";
                    mp.MoveFigure(-3);
                    break;
                case "<color=grey>Hitman</color>":                                                  //implemented :D
                    int unlucky = Random.Range(0, 4);
                    int negSpaces = Random.Range(3, 6);
                    movementText.text = "Player " + (unlucky + 1) + " will move back " + negSpaces + " spaces! Hard luck...";
                    switch (unlucky)
                    {
                        case 0:
                            mp1.MoveFigure(-negSpaces);
                            break;
                        case 1:
                            mp2.MoveFigure(-negSpaces);
                            break;
                        case 2:
                            mp3.MoveFigure(-negSpaces);
                            break;
                        case 3:
                            mp4.MoveFigure(-negSpaces);
                            break;
                    }
                    break;
                case "<color=yellow>Minigame</color>":                                           //Needs minigames :c
                    movementText.text = "You would play a random minigame, not implemented yet :(";
                    break;
                case "<color=blue>Testimony</color>":                                          //implemented :D
                    int choice = Random.Range(0, 2);
                    switch (choice)
                    {
                        case 0:
                            movementText.text = "TESTIMONY! you move forward by five tiles! Lucky you!";
                            mp.MoveFigure(5);
                            break;
                        case 1:
                            movementText.text = "TESTIMONY! you move back by FIVE tiles! Hard luck pal...";
                            mp.MoveFigure(-5);
                            break;
                    }
                    break;
                case "<color=orange>Interrogation</color>":                                     //implemented :D
                    movementText.text = "The head of police wants to hear your progress! You miss 2 turns!\nNot implemented yet, sorry for the inconvenience...";
                    /*skipTurns[player] = 2;
                    currentPlayer++;*/
                    break;
            }
        }
        inTileEffects = false;
    }

    public void LaunchFWMinigame()
    {
        launchFWMinigame.SetActive(false);
        SceneManager.LoadScene("FindWaldo Lobby", LoadSceneMode.Additive);
        Cursor.lockState = CursorLockMode.None;
        mp.isLoaded = true;
        inMinigame = true;
        textBox.SetActive(false);
        fieldDisplay.gameObject.transform.parent.gameObject.SetActive(false);
        board.SetActive(false);
        inTileEffects = false;
    }

    public void SkipIntro()
    {
        introIndex = introText.Length;
        movementText.text = introText[introIndex - 1];
    }

    private void PlayTurn(int player)
    {
        inRound = true;
        if (Input.GetKeyDown(KeyCode.Space) && gameIndex < 8 && !inMinigame && mp.isMoving == false)
        {
            Cursor.lockState = CursorLockMode.Locked;

            if (skipTurns[player] == 0)
            {
                //Action to be made depending on the stage of the current turn.
                switch (gameIndex)
                {
                    case 0:
                        break;
                    case 1:
                        newRound = false;
                        textBox.SetActive(false);
                        roundCounter.SetActive(true);
                        roundDisplay.text = "Player " + (player + 1) + "'s turn!";
                        break;
                    case 2:
                        roundCounter.SetActive(false);
                        textBox.SetActive(true);
                        movementText.text = "Alright, time to hop into the minigame to decide how far you can go!";
                        break;
                    case 3:
                        textBox.SetActive(false);
                        nextFWDisplay.gameObject.transform.parent.gameObject.SetActive(false);
                        fieldDisplay.gameObject.transform.parent.gameObject.SetActive(false);
                        board.SetActive(false);
                        SceneManager.LoadScene("racingMenu", LoadSceneMode.Additive);
                        inMinigame = true;
                        break;
                    case 4:
                        textBox.SetActive(true);

                        if (completion > 60.0f)
                        {
                            movementText.text = "Congrats! You have completed the minigame to " + completion.ToString() + "%!\n You will move forward " + advance + " spaces!";
                        }
                        else
                        {
                            if (advance == 1)
                            {
                                movementText.text = "You have completed the minigame to " + completion.ToString() + "%.\n You will move forward " + advance + " space!";
                            }
                            else
                            {
                                movementText.text = "You have completed the minigame to " + completion.ToString() + "%.\n You will move forward " + advance + " spaces!";
                            }
                        }
                        break;
                    case 5:
                        textBox.SetActive(false);
                        Cursor.lockState = CursorLockMode.None;
                        fieldDisplay.gameObject.transform.parent.gameObject.SetActive(true);
                        nextFWDisplay.gameObject.transform.parent.gameObject.SetActive(true);
                        mp.MoveFigure(advance);
                        textBox.SetActive(true);
                        movementText.text = "";
                        break;
                    case 6:
                        movementText.text = "You have landed on a " + tileEffects[mp.routePosition] + " tile!";
                        inTileEffects = true;
                        PlayTileEffects(mp.routePosition, player);
                        break;
                    case 7:
                        if(currentPlayer == 4)
                        {
                            switch (currentRound)
                            {
                                case 1:
                                case 21:
                                case 31:
                                    movementText.text = "Your " + currentRound.ToString() + "st turn is over.\nnext up is player 1";
                                    break;
                                case 2:
                                case 22:
                                case 32:
                                    movementText.text = "Your " + currentRound.ToString() + "nd turn is over.\nnext up is player 1";
                                    break;
                                case 3:
                                case 23:
                                case 33:
                                    movementText.text = "Your " + currentRound.ToString() + "rd turn is over.\nnext up is player 1";
                                    break;
                                default:
                                    movementText.text = "Your " + currentRound.ToString() + "th turn is over.\nnext up is player 1";
                                    break;
                            }
                        }else
                        {
                            switch (currentRound)
                            {
                                case 1:
                                case 21:
                                case 31:
                                    movementText.text = "Your " + currentRound.ToString() + "st turn is over.\nnext up is player " + (currentPlayer + 1);
                                    break;
                                case 2:
                                case 22:
                                case 32:
                                    movementText.text = "Your " + currentRound.ToString() + "nd turn is over.\nnext up is player " + (currentPlayer + 1);
                                    break;
                                case 3:
                                case 23:
                                case 33:
                                    movementText.text = "Your " + currentRound.ToString() + "rd turn is over.\nnext up is player " + (currentPlayer + 1);
                                    break;
                                default:
                                    movementText.text = "Your " + currentRound.ToString() + "th turn is over.\nnext up is player " + (currentPlayer + 1);
                                    break;
                            }
                        }

                        if (currentPlayer == 4)
                        {
                            currentRound++;
                            currentPlayer = 1;
                        }
                        else currentPlayer++;
                        Cursor.lockState = CursorLockMode.Locked;
                        break;
                }
            }
            else
            {
                movementText.text = "You still have " + skipTurns[currentPlayer - 1] + " turns to miss. Better luck next time!";
                skipTurns[currentPlayer - 1]--;
                if(currentPlayer == 4)
                {
                    currentRound++;
                    currentPlayer = 1;
                } else
                {
                    currentPlayer++;
                }
            }

            if (gameIndex == 7)
            {
                gameIndex = 1;
            }
            else gameIndex++;
        }
    }

    private void SimTurn(int player)
    {
        inRound = true;
        if (Input.GetKeyDown(KeyCode.Space) && gameIndex < 8 && !inMinigame && mp.isMoving == false)
        {
            Cursor.lockState = CursorLockMode.Locked;
            if (skipTurns[player - 1] == 0)
            {
                //Action to be made depending on the stage of the current turn.
                switch (gameIndex)
                {
                    case 0:
                        break;
                    case 1:
                        textBox.SetActive(false);
                        roundCounter.SetActive(true);
                        roundDisplay.text = "Player " + (player + 1) + "(CPU)'s turn!";
                        break;
                    case 2:
                        roundCounter.SetActive(false);
                        textBox.SetActive(true);
                        movementText.text = "Alright, time to hop into the minigame to decide how far you can go!";
                        break;
                    case 3:
                        movementText.text = "CPU is playing the minigame...";
                        moveDist = CalculateAIAdvance(player);
                        Debug.Log(moveDist);
                        break;
                    case 4:
                        textBox.SetActive(true);
                        movementText.text = "CPU will move forward " + moveDist + " spaces!";
                        break;
                    case 5:
                        textBox.SetActive(false);
                        fieldDisplay.gameObject.transform.parent.gameObject.SetActive(true);
                        mp.MoveFigure(moveDist);
                        textBox.SetActive(true);
                        movementText.text = "";
                        break;
                    case 6:
                        movementText.text = "CPU has landed on a " + tileEffects[mp.routePosition] + " tile!";
                        inTileEffects = true;
                        PlayTileEffects(mp.routePosition, player);
                        break;
                    case 7:
                        if (currentPlayer == 4)
                        {
                            switch (currentRound)
                            {
                                case 1:
                                case 21:
                                case 31:
                                    movementText.text = "CPU's " + currentRound.ToString() + "st turn is over.\nnext up is player 1";
                                    break;
                                case 2:
                                case 22:
                                case 32:
                                    movementText.text = "CPU's " + currentRound.ToString() + "nd turn is over.\nnext up is player 1";
                                    break;
                                case 3:
                                case 23:
                                case 33:
                                    movementText.text = "CPU's " + currentRound.ToString() + "rd turn is over.\nnext up is player 1";
                                    break;
                                default:
                                    movementText.text = "CPU's " + currentRound.ToString() + "th turn is over.\nnext up is player 1";
                                    break;
                            }
                        }
                        else
                        {
                            switch (currentRound)
                            {
                                case 1:
                                case 21:
                                case 31:
                                    movementText.text = "CPU's " + currentRound.ToString() + "st turn is over.\nnext up is player " + (currentPlayer + 1);
                                    break;
                                case 2:
                                case 22:
                                case 32:
                                    movementText.text = "CPU's " + currentRound.ToString() + "nd turn is over.\nnext up is player " + (currentPlayer + 1);
                                    break;
                                case 3:
                                case 23:
                                case 33:
                                    movementText.text = "CPU's " + currentRound.ToString() + "rd turn is over.\nnext up is player " + (currentPlayer + 1);
                                    break;
                                default:
                                    movementText.text = "CPU's " + currentRound.ToString() + "th turn is over.\nnext up is player " + (currentPlayer + 1);
                                    break;
                            }
                        }
                        if (currentPlayer == 4)
                        {
                            currentRound++;
                            currentPlayer = 1;
                        }
                        else currentPlayer++;
                        break;
                }
            }
            else
            {
                movementText.text = "CPU still has " + skipTurns[player] + " turns to miss. Better luck next time!";
                skipTurns[player]--;
                if (currentPlayer == 4)
                {
                    currentRound++;
                    currentPlayer = 1;
                }
                else
                {
                    currentPlayer++;
                }
            }

            if (gameIndex == 7)
            {
                gameIndex = 1;
            }
            else gameIndex++;
        }
    }

    private int CalculateAIAdvance(int player)
    {
        int distanceToFW = findWaldoTiles[findWaldoCounter[player]] - mp.routePosition;
        if(distanceToFW <= 0)
        {
            findWaldoCounter[player]++;
            distanceToFW = findWaldoTiles[findWaldoCounter[player - 1]] - mp.routePosition;
        }

        int moveDistance = 0;
        maxAdvance = Random.Range(4, 9);
        Debug.Log(maxAdvance);
        switch (difficulty)
        {
            case "easy":
                float advancement = Random.Range(0.0f, 1.0f);
                if (distanceToFW < maxAdvance)
                {
                    if (advancement <= 0.2)                                             //20% chance of landing on a find Waldo Tile
                    {
                        moveDistance = distanceToFW;
                    } else
                    {
                        if (advancement <= 0.15)                                        //15% chance of 0-20% completion
                        {
                            float rndCompletion = Random.Range(0.0f, 0.2f);
                            Debug.Log(rndCompletion);
                            moveDistance = (int)(rndCompletion * maxAdvance);
                        }
                        else if (advancement > 0.15 && advancement <= 0.45)             //30% chance of 20-40% completion
                        {
                            float rndCompletion = Random.Range(0.2f, 0.4f);
                            Debug.Log(rndCompletion);
                            moveDistance = (int)(rndCompletion * maxAdvance);
                        }
                        else if (advancement > 0.4 && advancement <= 0.75)              //35% chance of 40-60% completion
                        {
                            float rndCompletion = Random.Range(0.4f, 0.6f);
                            Debug.Log(rndCompletion);
                            moveDistance = (int)(rndCompletion * maxAdvance);
                        }
                        else if (advancement > 0.75 && advancement <= 0.9)              //15% chance of 60-80% completion
                        {
                            float rndCompletion = Random.Range(0.6f, 0.8f);
                            Debug.Log(rndCompletion);
                            moveDistance = (int)(rndCompletion * maxAdvance);
                        }
                        else if (advancement > 0.9)                                     //10% chance of 80-100% completion
                        {
                            float rndCompletion = Random.Range(0.8f, 1.0f);
                            Debug.Log(rndCompletion);
                            moveDistance = (int)(rndCompletion * maxAdvance);
                        }
                    }
                } else
                {
                    if(advancement <= 0.15)
                    {
                        float rndCompletion = Random.Range(0.0f, 0.2f);
                        Debug.Log(rndCompletion);
                        moveDistance = (int)(rndCompletion * maxAdvance);
                    } else if (advancement > 0.15 && advancement <= 0.45)
                    {
                        float rndCompletion = Random.Range(0.2f, 0.4f);
                        Debug.Log(rndCompletion);
                        moveDistance = (int)(rndCompletion * maxAdvance);
                    } else if (advancement > 0.45 && advancement <= 0.75)
                    {
                        float rndCompletion = Random.Range(0.4f, 0.6f);
                        Debug.Log(rndCompletion);
                        moveDistance = (int)(rndCompletion * maxAdvance);
                    } else if (advancement > 0.75 && advancement <= 0.9)
                    {
                        float rndCompletion = Random.Range(0.6f, 0.8f);
                        Debug.Log(rndCompletion);
                        moveDistance = (int)(rndCompletion * maxAdvance);
                    } else if (advancement > 0.9)
                    {
                        float rndCompletion = Random.Range(0.8f, 1.0f);
                        Debug.Log(rndCompletion);
                        moveDistance = (int)(rndCompletion * maxAdvance);
                    }
                }
                break;
            case "medium":
                advancement = Random.Range(0.0f, 1.0f);
                if (distanceToFW < maxAdvance)
                {
                    if (advancement <= 0.35)
                    {
                        moveDistance = distanceToFW;
                    }
                    else
                    {
                        if (advancement <= 0.1)
                        {
                            float rndCompletion = Random.Range(0.0f, 0.2f);
                            Debug.Log(rndCompletion);
                            moveDistance = (int)(rndCompletion * maxAdvance);
                        }
                        else if (advancement > 0.1 && advancement <= 0.25)
                        {
                            float rndCompletion = Random.Range(0.2f, 0.4f);
                            Debug.Log(rndCompletion);
                            moveDistance = (int)(rndCompletion * maxAdvance);
                        }
                        else if (advancement > 0.25 && advancement <= 0.6)
                        {
                            float rndCompletion = Random.Range(0.4f, 0.6f);
                            Debug.Log(rndCompletion);
                            moveDistance = (int)(rndCompletion * maxAdvance);
                        }
                        else if (advancement > 0.6 && advancement <= 0.85)
                        {
                            float rndCompletion = Random.Range(0.6f, 0.8f);
                            Debug.Log(rndCompletion);
                            moveDistance = (int)(rndCompletion * maxAdvance);
                        }
                        else if (advancement > 0.85)
                        {
                            float rndCompletion = Random.Range(0.8f, 1.0f);
                            Debug.Log(rndCompletion);
                            moveDistance = (int)(rndCompletion * maxAdvance);
                        }
                    }
                }
                else
                {
                    if (advancement <= 0.1)
                    {
                        float rndCompletion = Random.Range(0.0f, 0.2f);
                        Debug.Log(rndCompletion);
                        moveDistance = (int)(rndCompletion * maxAdvance);
                    }
                    else if (advancement > 0.1 && advancement <= 0.25)
                    {
                        float rndCompletion = Random.Range(0.2f, 0.4f);
                        Debug.Log(rndCompletion);
                        moveDistance = (int)(rndCompletion * maxAdvance);
                    }
                    else if (advancement > 0.25 && advancement <= 0.6)
                    {
                        float rndCompletion = Random.Range(0.4f, 0.6f);
                        Debug.Log(rndCompletion);
                        moveDistance = (int)(rndCompletion * maxAdvance);
                    }
                    else if (advancement > 0.6 && advancement <= 0.85)
                    {
                        float rndCompletion = Random.Range(0.6f, 0.8f);
                        Debug.Log(rndCompletion);
                        moveDistance = (int)(rndCompletion * maxAdvance);
                    }
                    else if (advancement > 0.85)
                    {
                        float rndCompletion = Random.Range(0.8f, 1.0f);
                        Debug.Log(rndCompletion);
                        moveDistance = (int)(rndCompletion * maxAdvance);
                    }
                }
                break;
            case "hard":
                advancement = Random.Range(0.0f, 1.0f);
                if (distanceToFW < maxAdvance)
                {
                    if (advancement <= 0.5)
                    {
                        moveDistance = distanceToFW;
                    }
                    else
                    {
                        if (advancement <= 0.1)
                        {
                            float rndCompletion = Random.Range(0.0f, 0.2f);
                            Debug.Log(rndCompletion);
                            moveDistance = (int)(rndCompletion * maxAdvance);
                        }
                        else if (advancement > 0.1 && advancement <= 0.25)
                        {
                            float rndCompletion = Random.Range(0.2f, 0.4f);
                            Debug.Log(rndCompletion);
                            moveDistance = (int)(rndCompletion * maxAdvance);
                        }
                        else if (advancement > 0.25 && advancement <= 0.6)
                        {
                            float rndCompletion = Random.Range(0.4f, 0.6f);
                            Debug.Log(rndCompletion);
                            moveDistance = (int)(rndCompletion * maxAdvance);
                        }
                        else if (advancement > 0.60 && advancement <= 0.85)
                        {
                            float rndCompletion = Random.Range(0.6f, 0.8f);
                            Debug.Log(rndCompletion);
                            moveDistance = (int)(rndCompletion * maxAdvance);
                        }
                        else if (advancement > 0.85)
                        {
                            float rndCompletion = Random.Range(0.80f, 1.0f);
                            Debug.Log(rndCompletion);
                            moveDistance = (int)(rndCompletion * maxAdvance);
                        }
                    }
                }
                else
                {
                    if (advancement <= 0.1)
                    {
                        float rndCompletion = Random.Range(0.0f, 0.2f);
                        Debug.Log(rndCompletion);
                        moveDistance = (int)(rndCompletion * maxAdvance);
                    }
                    else if (advancement > 0.1 && advancement <= 0.25)
                    {
                        float rndCompletion = Random.Range(0.2f, 0.4f);
                        Debug.Log(rndCompletion);
                        moveDistance = (int)(rndCompletion * maxAdvance);
                    }
                    else if (advancement > 0.25 && advancement <= 0.6)
                    {
                        float rndCompletion = Random.Range(0.4f, 0.6f);
                        Debug.Log(rndCompletion);
                        moveDistance = (int)(rndCompletion * maxAdvance);
                    }
                    else if (advancement > 0.60 && advancement <= 0.85)
                    {
                        float rndCompletion = Random.Range(0.6f, 0.8f);
                        Debug.Log(rndCompletion);
                        moveDistance = (int)(rndCompletion * maxAdvance);
                    }
                    else if (advancement > 0.85)
                    {
                        float rndCompletion = Random.Range(0.80f, 1.0f);
                        Debug.Log(rndCompletion);
                        moveDistance = (int)(rndCompletion * maxAdvance);
                    }
                }
                break;
        }
        if (moveDistance == 0)
        {
            moveDistance = 1;
        }

        return moveDistance;
    }
}
