﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DJ : MonoBehaviour
{
    private AudioSource camSound;

    public AudioClip[] music;

    private int songIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        camSound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!camSound.isPlaying)
        {
            Debug.Log(music[songIndex].name);
            camSound.PlayOneShot(music[songIndex]);

            switch (songIndex)
            {
                case 0:
                case 1:
                case 2:
                    songIndex++;
                    break;
                case 3:
                    songIndex = 0;
                    break;
            }
        }
    }
}
