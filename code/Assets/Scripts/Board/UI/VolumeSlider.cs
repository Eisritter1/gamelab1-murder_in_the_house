﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeSlider : MonoBehaviour
{
    private void OnEnable()
    {
        GetComponent<Slider>().value = AudioListener.volume;
    }
}
