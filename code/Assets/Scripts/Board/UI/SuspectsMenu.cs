﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SuspectsMenu : MonoBehaviour
{
    //Scripts:

    private GameManager gm;

    //UI Elements:

    public GameObject suspectsMenu;

    public GameObject cluesPage;

    public GameObject suspectsPage;

    public Image panel;

    //General:

    private bool pauseMenu = false;

    // Start is called before the first frame update
    void Start()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();

        suspectsPage.transform.Find("Candace").GetComponent<Image>().color = Color.white;
        suspectsPage.transform.Find("Steve").GetComponent<Image>().color = Color.white;
        suspectsPage.transform.Find("Karen").GetComponent<Image>().color = Color.white;
        suspectsPage.transform.Find("Stabby Mike").GetComponent<Image>().color = Color.white;
        suspectsPage.transform.Find("Sir Winston").GetComponent<Image>().color = Color.white;
        suspectsPage.transform.Find("Tom").GetComponent<Image>().color = Color.white;
    }

    // Update is called once per frame
    void Update()
    {
        //Opens Suspects & clues menu
        if (Input.GetKeyDown(KeyCode.Mouse1) && !gm.paused && !gm.inMinigame)
        {
            OpenSupectsMenu();
        }
        else if (gm.inSusMenu && Input.GetKeyDown(KeyCode.Mouse1) && gm.paused)
        {
            CloseSuspectsMenu();
        }
    }



    //Function to go to the suspects and clues menu:
    public void OpenSupectsMenu()
    {
        if (gm.paused)
        {
            gm.pauseMenu.SetActive(false);
            pauseMenu = true;
        }

        gm.paused = true;
        gm.inSusMenu = true;
        suspectsMenu.SetActive(true);
        Cursor.lockState = CursorLockMode.None;

        Debug.Log("Opening Suspects Menu");

        cluesPage.SetActive(false);
        suspectsPage.SetActive(true);
    }

    //Function to go from suspects overview to clues overview:
    public void ChangeToClues()
    {
        cluesPage.SetActive(true);
        suspectsPage.SetActive(false);
    }

    //Function to go from clues overview to suspects overview:
    public void BackToSuspects()
    {
        cluesPage.SetActive(false);
        suspectsPage.SetActive(true);
    }

    //Function to close suspects & clues menu:
    public void CloseSuspectsMenu()
    {
        cluesPage.SetActive(false);
        suspectsPage.SetActive(true);
        suspectsMenu.SetActive(false);

        Debug.Log("Closing Suspects Menu");

        gm.inSusMenu = false;
        gm.paused = false;

        if (pauseMenu)
        {
            pauseMenu = false;
            gm.pauseMenu.SetActive(true);
        } else if (!pauseMenu)
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
    }

    //Function to change suspect card color to green/red:
    public void SelectSuspects()
    {
        Debug.Log(UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name);
        panel = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.transform.parent.GetComponent<Image>();
        if (panel.color == Color.white)
        {
            panel.color = Color.green;
        } else if (panel.color == Color.green)
        {
            panel.color = Color.white;
        }
    }
}
