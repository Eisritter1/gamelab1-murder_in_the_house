﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{
    public GameObject pauseMenu;

    public GameObject settingsMenu;

    public AudioMixer audioMixer;

    public Slider volumeSlider;

    public void SetVolume(float volume)
    {
        Debug.Log (volume);
        AudioListener.volume = volume;
    }

    public void OpenSettings()
    {
        pauseMenu.SetActive(false);
        settingsMenu.SetActive(true);
    }

    public void CloseSettings()
    {
        pauseMenu.SetActive(true);
        settingsMenu.SetActive(false);
    }
}
