﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Clues : MonoBehaviour
{
    //Clues Creation:

    public Texture[] clues;

    public string[] suspects;

    public string[] innocents;

    private bool accusation = false;

    private bool fingerprint = false;

    //Clue card mechanic:

    public Texture[,] unlockedClueTex;

    public Texture[] lockedClueTex;

    public RawImage[] clueImages;

    public Text[] clueDescriptions;

    public string[] lockedClueDesc;

    public string[,] unlockedClueDesc;

    public int[] cluesCounter;

    //Script Communication:

    GameManager gm;

    // Start is called before the first frame update
    void Start()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();

        unlockedClueTex = new Texture[4, 4];

        lockedClueTex = new Texture[4];

        lockedClueDesc = new string[4];

        unlockedClueDesc = new string[4, 4];

        cluesCounter = new int[4];

        suspects = new string[] { "Candace", "Steve", "Karen", "Mike", "Sir Winston", "Tom" };

        innocents = new string[5];

        int j = 0;

        for (int i = 0; i < suspects.Length; i++)
        {
            if (suspects[i] != gm.culprit)
            {
                if (j < innocents.Length)
                {
                    innocents[j] = suspects[i];
                    j++;
                }
            }
        }

        for (int i = 0; i < 4; i++)
        {
            cluesCounter[i] = 0;
            for (int k = 0; k < 4; k++)
            {
                unlockedClueTex[i, k] = null;
                unlockedClueDesc[i, k] = "";
            }
        }

        ChooseClues();

        for (int i = 0; i < lockedClueTex.Length; i++)
        {
            Debug.Log(lockedClueTex[i].name);
        }
    }

    private void Update()
    {
        if (cluesCounter[gm.currentPlayer - 1] > 0)
        {
            for (int i = 0; i < cluesCounter[gm.currentPlayer - 1]; i++)
            {
                clueImages[i].texture = unlockedClueTex[gm.currentPlayer - 1, i];
                clueDescriptions[i].text = unlockedClueDesc[gm.currentPlayer - 1, i];
            }
        }
    }

    //Clues order: 0 - Dagger; 1 - Poison; 2 - Gun; 3 - Rope; 4 - Wrench; 5 - Note; 6 - Fingerprint; 7 - question mark

    //Function to make up some clues for the game:
    private void ChooseClues()
    {
        int i = 0;
        while (i < 4)
        {
            //depending on the number of current clue...
            switch (i)
            {   //Clue no. 1 is the murder weapon, so it is chosen accordingly.
                case 0:
                    switch (gm.murderWeapon)
                    {
                        case "dagger":
                            lockedClueTex[i] = clues[0];
                            lockedClueDesc[i] = "A dagger with traces of Blood and a 12 cm blade. Used to stab Sir Joe";
                            break;
                        case "poison":
                            lockedClueTex[i] = clues[1];
                            lockedClueDesc[i] = "A bottle containing highly toxic chemical cyanide. Traces of said poison found in Sir Joe's glass";
                            break;
                        case "gun":
                            lockedClueTex[i] = clues[2];
                            lockedClueDesc[i] = "A handgun found at the scene of the crime. Fires 9mm bullets, fits to the ones used for the murder";
                            break;
                        case "rope":
                            lockedClueTex[i] = clues[3];
                            lockedClueDesc[i] = "A sturdy rope found at the scene of the crime. Traces of a rope reported around the victim's neck";
                            break;
                        case "wrench":
                            lockedClueTex[i] = clues[4];
                            lockedClueDesc[i] = "A wrench with traces of blood found at the crime scene. Culprit smashed the victim's head with it...";
                            break;
                        default:
                            Debug.Log("Failed to find a murder weapon");
                            break;
                    }
                    break;
                //Clues no. 2-4 are testimonies and maybe (with 20% chance) a fingerprint:
                case 1:
                case 2:
                case 3:
                    int choice = Random.Range(1, 11);
                    int testifier = Random.Range(0, 5);
                    switch (choice)
                    {   //80% chance of a testimony
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                        case 7:
                        case 8:
                            lockedClueTex[i] = clues[5];
                            //If there hasn't already been an accusative clue, there shall be one.
                            if (!accusation)
                            {
                                int acc = Random.Range(1, 4);
                                switch (acc)
                                {   //Diverse accusations, just for visual pleasure...
                                    case 1:
                                        switch (gm.culprit)
                                        {
                                            case "Candace":
                                            case "Steve":
                                            case "Karen":
                                                lockedClueDesc[i] = "Testimony from " + innocents[testifier] + ": At dinner there was a huge argument at the table! Something over a family issue. I didn't quite understand everything though...";
                                                break;
                                            case "Sir Winston":
                                            case "Tom":
                                                lockedClueDesc[i] = "Testimony from " + innocents[testifier] + ": At dinner there was a huge argument at the table! Something over a business issue. I didn't quite understand everything though...";
                                                break;
                                            case "Mike":
                                                lockedClueDesc[i] = "Testimony from " + innocents[testifier] + ": At dinner there was a huge argument at the table! Something over a trust issue. I didn't quite understand everything though...";
                                                break;
                                        }
                                        break;
                                    case 2:
                                        lockedClueDesc[i] = "Testimony from " + innocents[testifier] + ": " + gm.culprit + "just disappeared for a while, and 20 minutes later the body was found... Maybe it was just a toilet call though.";
                                        break;
                                    case 3:
                                        if(gm.murderWeapon == "poison")
                                        {
                                            lockedClueDesc[i] = "Testimony from " + innocents[testifier] + ": Joe collapsed on the table in the middle of dinner! As I was looking around, " + gm.culprit + "didn't look very surprised...";
                                        } else
                                        {
                                            lockedClueDesc[i] = "Testimony from " + innocents[testifier] + ": I found " + gm.culprit + "'s behaviour a bit weird after the body was found... he seemed very... rushed and discomposed...";
                                        }
                                        break;
                                }
                                accusation = true;
                            }   //Otherwise it's an innocentiation (a random innocent tells you another person is innocent.
                            else //May want to add variation so in 5% of cases the murderer gives a false account?
                            {
                                int phrase = Random.Range(1, 4);
                                int innocented = testifier;

                                while (innocented == testifier)
                                {
                                    innocented = Random.Range(0, 5);
                                }

                                switch (phrase)
                                {
                                    case 1:
                                        lockedClueDesc[i] = "Testimony from " + innocents[testifier] + ": I was talking with " + innocents[innocented] + "about the Napoleonic wars virtually all evening, I really can't see them killing Joe...";
                                        break;
                                    case 2:
                                        lockedClueDesc[i] = "Testimony from " + innocents[testifier] + ": " + innocents[innocented] + " was in the dining room all evening. Didn't leave it until the body was found. ";
                                        break;
                                    case 3:
                                        int game = Random.Range(1, 4);
                                        switch (game)
                                        {
                                            case 1:
                                                lockedClueDesc[i] = "Testimony from " + innocents[testifier] + ": When the body was found, " + innocents[innocented] + " and I were in a tense round of bridge, and we saw Joe before we started.";
                                                break;
                                            case 2:
                                                lockedClueDesc[i] = "Testimony from " + innocents[testifier] + ": When the body was found, " + innocents[innocented] + " and I were in a tense round of chess, and we saw Joe before we started.";
                                                break;
                                            case 3:
                                                lockedClueDesc[i] = "Testimony from " + innocents[testifier] + ": When the body was found, " + innocents[innocented] + " and I were in a tense round of scrabble, and we saw Joe before we started.";
                                                break;
                                        }
                                        break;
                                }
                            }
                            break;
                        case 9:
                        case 10:
                            //20% chance of a fingerprint;
                            if (!fingerprint)
                            {
                                lockedClueTex[i] = clues[6];
                            } else //if there already is a fingerprint in the game, it will give another testimony...
                            {
                                int phrase = Random.Range(1, 4);
                                int innocented = testifier;

                                while (innocented == testifier)
                                {
                                    innocented = Random.Range(0, 5);
                                }

                                switch (phrase)
                                {
                                    case 1:
                                        lockedClueDesc[i] = "Testimony from " + innocents[testifier] + ": I was talking with " + innocents[innocented] + "about the Napoleonic wars virtually all evening, I really can't see them killing Joe...";
                                        break;
                                    case 2:
                                        lockedClueDesc[i] = "Testimony from " + innocents[testifier] + ": " + innocents[innocented] + " was in the dining room all evening. Didn't leave it until the body was found. ";
                                        break;
                                    case 3:
                                        int game = Random.Range(1, 4);
                                        switch (game)
                                        {
                                            case 1:
                                                lockedClueDesc[i] = "Testimony from " + innocents[testifier] + ": When the body was found, " + innocents[innocented] + " and I were in a tense round of bridge, and we saw Joe before we started.";
                                                break;
                                            case 2:
                                                lockedClueDesc[i] = "Testimony from " + innocents[testifier] + ": When the body was found, " + innocents[innocented] + " and I were in a tense round of chess, and we saw Joe before we started.";
                                                break;
                                            case 3:
                                                lockedClueDesc[i] = "Testimony from " + innocents[testifier] + ": When the body was found, " + innocents[innocented] + " and I were in a tense round of scrabble, and we saw Joe before we started.";
                                                break;
                                        }
                                        break;
                                }
                            }
                            break;
                    }
                    break;
            }
            i++;
        }
        
    }

    //Function used upon discovery of a clue:
    public void DiscoverClue(int player)
    {
        unlockedClueTex[player - 1, cluesCounter[player - 1]] = lockedClueTex[cluesCounter[player - 1]];                                    //adds the found clue to the player's unlockedClues array
        unlockedClueDesc[player - 1, cluesCounter[player - 1]] = lockedClueDesc[cluesCounter[player - 1]];                                  
        cluesCounter[player - 1]++;                                                                                                         //increments the player's clueCounter
    }
}
