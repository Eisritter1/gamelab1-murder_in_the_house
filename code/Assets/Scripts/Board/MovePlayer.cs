﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MovePlayer : MonoBehaviour {

    private GameManager gm;

    public Route currentRoute;

    public int routePosition;

    public int steps;

    public bool isMoving;

    public bool isLoaded;

    private bool minigameTiming;

    int mgIndex;

    //Scripts:

    private MouseLook ml;
                                                                                                                 
    private void Start()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();

        ml = gm.ml;

        Debug.Log(currentRoute.childFieldList.Count);
    }

    void Update() 
    {
        if (isMoving == true)
        {
            minigameTiming = false;
        }

        else if (isMoving == false)
        {
            minigameTiming = true;
        }
    }
    
    public IEnumerator Move(int steps){

        if (isMoving){

            yield break;

        }

        isMoving = true;

        if (steps > 0)
        {
            while (steps > 0){

                routePosition++;
                routePosition %= currentRoute.childFieldList.Count;

                Vector3 nextPos = currentRoute.childFieldList[routePosition].position;

                while (MoveToNextField(nextPos))
                {
                    gm.textBox.gameObject.SetActive(false);
                    ml.enabled = true;
                    yield return null;
                }

                yield return new WaitForSeconds(0.1f);
                gm.textBox.gameObject.SetActive(true);
                steps--;
            }
        }

        else if (steps < 0)
        {
            while (steps < 0 && routePosition > 0)
            {
                routePosition--;
                routePosition %= currentRoute.childFieldList.Count;

                Vector3 nextPos = currentRoute.childFieldList[routePosition].position;

                while (MoveToNextField(nextPos))
                {
                    gm.textBox.gameObject.SetActive(false);
                    ml.enabled = true;
                    yield return null;
                }

                yield return new WaitForSeconds(0.1f);
                gm.textBox.gameObject.SetActive(true);
                steps++;
            }
        }

        isMoving = false;

        if(gm.racingManager)
        {
            Destroy(gm.racingManager);
        }

    }

    bool MoveToNextField(Vector3 target){

        return target != (transform.position = Vector3.MoveTowards(transform.position, target, 5f * Time.deltaTime));
    }

    public void MoveFigure(int moves)
    {
        gm.moving = true;
        steps = moves;
        Debug.Log("Based on your performance, you can move " + steps + " field/s forward!");
        StartCoroutine(Move(steps));
        gm.moving = false;
    }


}
