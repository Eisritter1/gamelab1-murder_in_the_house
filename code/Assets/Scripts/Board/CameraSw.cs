﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSw : MonoBehaviour
{
    public GameObject hallCamera;
    public GameObject kitchenCamera;
    public GameObject diningRoomCamera;
    public GameObject labCamera;
    public GameObject opCamera;
    public GameObject corridorCamera;
    public GameObject basementCamera1;
    public GameObject basementCamera2;

    void Start()
    {
        hallCamera.SetActive(true);
        kitchenCamera.SetActive(false);
        diningRoomCamera.SetActive(false);
        labCamera.SetActive(false);
        opCamera.SetActive(false);
        corridorCamera.SetActive(false);
        basementCamera1.SetActive(false);
        basementCamera2.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown("0"))
        {
            hallCamera.SetActive(true);
            kitchenCamera.SetActive(false);
            diningRoomCamera.SetActive(false);
            labCamera.SetActive(false);
            opCamera.SetActive(false);
            corridorCamera.SetActive(false);
            basementCamera1.SetActive(false);
            basementCamera2.SetActive(false);
        }

        if (Input.GetKeyDown("1"))
        {
            hallCamera.SetActive(false);
            kitchenCamera.SetActive(true);
            diningRoomCamera.SetActive(false);
            labCamera.SetActive(false);
            opCamera.SetActive(false);
            corridorCamera.SetActive(false);
            basementCamera1.SetActive(false);
            basementCamera2.SetActive(false);
        }

        if (Input.GetKeyDown("2"))
        {
            hallCamera.SetActive(false);
            kitchenCamera.SetActive(false);
            diningRoomCamera.SetActive(true);
            labCamera.SetActive(false);
            opCamera.SetActive(false);
            corridorCamera.SetActive(false);
            basementCamera1.SetActive(false);
            basementCamera2.SetActive(false);
        }

        if (Input.GetKeyDown("3"))
        {
            hallCamera.SetActive(false);
            kitchenCamera.SetActive(false);
            diningRoomCamera.SetActive(false);
            labCamera.SetActive(true);
            opCamera.SetActive(false);
            corridorCamera.SetActive(false);
            basementCamera1.SetActive(false);
            basementCamera2.SetActive(false);
        }

        if (Input.GetKeyDown("4"))
        {
            hallCamera.SetActive(false);
            kitchenCamera.SetActive(false);
            diningRoomCamera.SetActive(false);
            labCamera.SetActive(false);
            opCamera.SetActive(true);
            corridorCamera.SetActive(false);
            basementCamera1.SetActive(false);
            basementCamera2.SetActive(false);
        }

        if (Input.GetKeyDown("5"))
        {
            hallCamera.SetActive(false);
            kitchenCamera.SetActive(false);
            diningRoomCamera.SetActive(false);
            labCamera.SetActive(false);
            opCamera.SetActive(false);
            corridorCamera.SetActive(true);
            basementCamera1.SetActive(false);
            basementCamera2.SetActive(false);
        }

        if (Input.GetKeyDown("6"))
        {
            hallCamera.SetActive(false);
            kitchenCamera.SetActive(false);
            diningRoomCamera.SetActive(false);
            labCamera.SetActive(false);
            opCamera.SetActive(false);
            corridorCamera.SetActive(false);
            basementCamera1.SetActive(true);
            basementCamera2.SetActive(false);
        }

        if (Input.GetKeyDown("7"))
        {
            hallCamera.SetActive(false);
            kitchenCamera.SetActive(false);
            diningRoomCamera.SetActive(false);
            labCamera.SetActive(false);
            opCamera.SetActive(false);
            corridorCamera.SetActive(false);
            basementCamera1.SetActive(false);
            basementCamera2.SetActive(true);
        }
    }
}



