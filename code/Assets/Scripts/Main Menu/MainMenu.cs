﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class MainMenu : MonoBehaviour
{
    public GameObject mainMenu;
    public GameObject playerChoice;
    public GameObject difficultyChoice;
    public GameObject credits;
    public GameObject quitGame;
    public GameObject startGame;
    public GameObject knownIssues;
    public Text title;

    public int players = 0;
    public string difficulty = "medium";

    private void Start()
    {
        DontDestroyOnLoad(this);
    }

    //go from main menu to player choice:
    public void SetupGame()
    {
        mainMenu.SetActive(false);
        playerChoice.SetActive(true);
    }

    //go from main menu to credits:
    public void OpenCredits()
    {
        mainMenu.SetActive(false);
        credits.SetActive(true);
        title.gameObject.SetActive(false);
    }

    //confirmation message to close the game:
    public void RequestExit()
    {
        quitGame.SetActive(true);
    }

    //close the game:
    public void ExitGame()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    //go from game setup back to main menu:
    public void StopSetup()
    {
        mainMenu.SetActive(true);
        playerChoice.SetActive(false);
        difficultyChoice.SetActive(false);
        startGame.SetActive(false);
    }

    //go from credits back to main menu:
    public void CloseCredits()
    {
        mainMenu.SetActive(true);
        credits.SetActive(false);
        title.gameObject.SetActive(true);
    }

    //abort game exit:
    public void DontExitGame()
    {
        quitGame.SetActive(false);
    }

    //if it's a single player:
    public void SinglePlayer()
    {
        players = 1;
        difficultyChoice.SetActive(true);
        playerChoice.SetActive(false);
    }

    //if two players:
    public void TwoPlayers()
    {
        players = 2;
        difficultyChoice.SetActive(true);
        playerChoice.SetActive(false);
    }

    //... I guess you notice the pattern now...
    public void ThreePlayers()
    {
        players = 3;
        difficultyChoice.SetActive(true);
        playerChoice.SetActive(false);
    }

    //...
    public void FourPlayers()
    {
        players = 4;
        playerChoice.SetActive(false);
        ConfirmStart();
    }

    //load the board and hop into the game:
    public void LaunchGame()
    {
        SceneManager.LoadScene("BoardInside");
    }

    //ask "ready to go?":
    public void ConfirmStart()
    {
        startGame.SetActive(true);
    }

    public void EasyDiff()
    {
        difficulty = "easy";
        ConfirmStart();
    }

    public void MediumDiff()
    {
        difficulty = "medium";
        ConfirmStart();
    }

    public void HardDiff()
    {
        difficulty = "hard";
        ConfirmStart();
    }

    public void OpenKnownIssues()
    {
        knownIssues.SetActive(true);
        mainMenu.SetActive(false);
    }

    public void CloseKnownIssues()
    {
        knownIssues.SetActive(false);
        mainMenu.SetActive(true);
    }
}
